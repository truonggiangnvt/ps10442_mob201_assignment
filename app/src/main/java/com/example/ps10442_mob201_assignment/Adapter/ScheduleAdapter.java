package com.example.ps10442_mob201_assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10442_mob201_assignment.R;
import com.example.ps10442_mob201_assignment.model.Schedule;

import java.util.ArrayList;

public class ScheduleAdapter extends  RecyclerView.Adapter<ScheduleAdapter.PersonViewHolder> {
    Context context;
    ArrayList<Schedule> list;
    public ScheduleAdapter(Context context, ArrayList<Schedule> list){
        this.context = context;
        this.list = list;
    }
    public class PersonViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtTeacher, txtRoom, txtStart;
        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtCourseName1);
            txtTeacher = itemView.findViewById(R.id.txtCourseTeacher1);
            txtRoom = itemView.findViewById(R.id.txtCourseRoom1);
            txtStart = itemView.findViewById(R.id.txtCourseStart1);
        }
    }
    @NonNull
    @Override
    public ScheduleAdapter.PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_schedule,null);
        ScheduleAdapter.PersonViewHolder pvh = new ScheduleAdapter.PersonViewHolder(view);
        return pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        final Schedule schedule = list.get(position);
        if (schedule != null){
            holder.txtName.setText(schedule.getCourseName());
            holder.txtTeacher.setText(schedule.getCourseTeacher());
            holder.txtRoom.setText(schedule.getCourseRoom());
            holder.txtStart.setText(schedule.getStartDay());
        }
    }
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
