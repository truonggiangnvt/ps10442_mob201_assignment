package com.example.ps10442_mob201_assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10442_mob201_assignment.DAO.CourseDAO;
import com.example.ps10442_mob201_assignment.DAO.ScheduleDAO;
import com.example.ps10442_mob201_assignment.R;
import com.example.ps10442_mob201_assignment.model.Course;
import com.example.ps10442_mob201_assignment.model.Schedule;

import java.util.ArrayList;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.PersonViewHolder> {
    Context context;
    ArrayList<Course> list;
    CourseDAO courseDAO;
    public CourseAdapter(Context context, ArrayList<Course> list){
        this.context = context;
        this.list = list;
    }
    public class PersonViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtTeacher, txtRoom, txtAmount, txtStartDay;
        Button btnChoose;
        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtNameCourse);
            txtTeacher = itemView.findViewById(R.id.txtTeacherCourse);
            txtAmount = itemView.findViewById(R.id.txtAmountCourse);
            btnChoose = itemView.findViewById(R.id.btnRegister);
        }
    }
    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cellcourse,null);
        PersonViewHolder pvh = new PersonViewHolder(view);
        return pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        final Course course = list.get(position);
        if (course!=null){
            holder.txtName.setText(course.getCourseName());
            holder.txtTeacher.setText(course.getCourseTeacher());
            holder.txtAmount.setText(String.valueOf(course.getCourseAmount()));
            holder.btnChoose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ScheduleDAO scheduleDAO = new ScheduleDAO(context);
                    Schedule schedule = new Schedule();
                    schedule.setCourseId(course.getCourseId());
                    schedule.setCourseName(course.getCourseName());
                    schedule.setCourseRoom(course.getCourseRoom());
                    schedule.setCourseTeacher(course.getCourseTeacher());
                    schedule.setStartDay(course.getStartDay());
                    schedule.setEndDay(course.getEndDay());
                    if (course.getCourseAmount()==0){
                        Toast.makeText(context,"Full Student, Please choose another course",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (scheduleDAO.addSchedule(schedule) == -1){
                        Toast.makeText(context,"You was register already",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(context,"Register is successfully",
                            Toast.LENGTH_SHORT).show();
                    courseDAO = new CourseDAO(context);
                    int amount = course.getCourseAmount()-1;
                    course.setCourseAmount(amount);
                    if (courseDAO.updateCourse(course)==-1){
                        return;
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
