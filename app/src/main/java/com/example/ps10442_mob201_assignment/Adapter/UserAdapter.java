package com.example.ps10442_mob201_assignment.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ps10442_mob201_assignment.R;
import com.example.ps10442_mob201_assignment.model.User;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends BaseAdapter {
    Context context;
    ArrayList<User> list;
    public UserAdapter(Context context,ArrayList<User> list){
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view==null){
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = ((Activity)context).getLayoutInflater();
            view = layoutInflater.inflate(R.layout.cell_watch_list_user,null);
            viewHolder.txtEmail = view.findViewById(R.id.txtEmailUser);
            viewHolder.txtName = view.findViewById(R.id.txtNameUser);
            viewHolder.txtPhone = view.findViewById(R.id.txtPhoneUser);
            viewHolder.txtDate = view.findViewById(R.id.txtDateUser);
            viewHolder.txtRole  = view.findViewById(R.id.txtRole);
            viewHolder.imageView = view.findViewById(R.id.circleImageUser);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        User user = list.get(i);
        viewHolder.txtEmail.setText(user.getEmailUser());
        viewHolder.txtName.setText(user.getNameUser());
        viewHolder.txtPhone.setText(user.getPhoneNumber()+"");
        viewHolder.txtDate.setText(user.getDateUser());
        viewHolder.txtRole.setText(user.getRole());

        return view;
    }
    class ViewHolder{
        TextView txtEmail, txtName, txtPhone, txtDate, txtRole;
        CircleImageView imageView;
    }
}
