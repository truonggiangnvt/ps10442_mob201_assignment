package com.example.ps10442_mob201_assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10442_mob201_assignment.R;
import com.example.ps10442_mob201_assignment.model.Course;
import com.example.ps10442_mob201_assignment.model.Schedule;

import java.util.ArrayList;

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.PersonViewHolder>  {
    Context context;
    ArrayList<Schedule> list;
    public ExamAdapter(Context context, ArrayList<Schedule> list){
        this.context = context;
        this.list = list;
    }
    public class PersonViewHolder extends RecyclerView.ViewHolder {
        TextView txtExamName, txtExamRoom, txtExamDay;
        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
            txtExamName = itemView.findViewById(R.id.txtExamName);
            txtExamRoom = itemView.findViewById(R.id.txtExamRoom);
            txtExamDay = itemView.findViewById(R.id.txtExamDay);
        }

    }
    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_exam,null);
        ExamAdapter.PersonViewHolder pvh = new ExamAdapter.PersonViewHolder(view);
        return pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        final Schedule schedule = list.get(position);
        if (schedule!=null){
            holder.txtExamDay.setText(schedule.getEndDay());
            holder.txtExamRoom.setText(schedule.getCourseRoom());
            holder.txtExamName.setText(schedule.getCourseName());
        }
    }
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
