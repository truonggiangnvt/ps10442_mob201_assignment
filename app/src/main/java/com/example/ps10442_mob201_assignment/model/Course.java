package com.example.ps10442_mob201_assignment.model;

public class Course {
    String courseId;
    String courseName;
    String courseTeacher;
    String courseRoom;
    String startDay;
    String endDay;
    int courseAmount;

    public Course(String courseId, String courseName, String courseTeacher, String courseRoom, String startDay, String endDay, int courseAmount) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseTeacher = courseTeacher;
        this.courseRoom = courseRoom;
        this.startDay = startDay;
        this.endDay = endDay;
        this.courseAmount = courseAmount;
    }
    public Course(){

    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseTeacher() {
        return courseTeacher;
    }

    public void setCourseTeacher(String courseTeacher) {
        this.courseTeacher = courseTeacher;
    }

    public String getCourseRoom() {
        return courseRoom;
    }

    public void setCourseRoom(String courseRoom) {
        this.courseRoom = courseRoom;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public String getEndDay() {
        return endDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }
    public void setCourseAmount(int courseAmount){
        this.courseAmount = courseAmount;
    }
    public int getCourseAmount(){
        return courseAmount;
    }
}
