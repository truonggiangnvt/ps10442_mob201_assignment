package com.example.ps10442_mob201_assignment.model;

public class User {
    String emailUser;
    String nameUser;
    String dateUser;
    int phoneNumber;
    String role;
    String url_image;
    public User(String emailUser, String nameUser, String dateUser, int phoneNumber, String role, String url_image) {
        this.emailUser = emailUser;
        this.nameUser = nameUser;
        this.dateUser = dateUser;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.url_image = url_image;
    }
    public User(){

    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getDateUser() {
        return dateUser;
    }

    public void setDateUser(String dateUser) {
        this.dateUser = dateUser;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
