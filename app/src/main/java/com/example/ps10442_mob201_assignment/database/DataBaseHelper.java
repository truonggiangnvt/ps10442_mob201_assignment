package com.example.ps10442_mob201_assignment.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBaseHelper extends SQLiteOpenHelper {
    public final static String DBNAME = "ManageCourse";
    public final static int DBVERSION = 1;
    public DataBaseHelper(Context context) {
        super(context, DBNAME, null , DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE USER(" + "EMAILUSER TEXT PRIMARY KEY," + "NAMEUSER TEXT NOT NULL,"+ "DATEUSER TEXT NOT NULL,"
                + "PHONEUSER INTEGER NOT NULL, " + "ROLEUSER TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);
        String sqll = "CREATE TABLE COURSE(" + "COURSEID TEXT PRIMARY KEY," + "COURSENAME TEXT NOT NULL," +
                "TEACHER TEXT NOT NULL," + "ROOM TEXT NOT NULL,"+ "AMOUNT INTEGER NOT NULL," + "STARTDAY TEXT NOT NULL," + "ENDDAY TEXT NOT NULL)";
       sqLiteDatabase.execSQL(sqll);
       String sql2 = "CREATE TABLE SCHEDULE(" + "COURSEID TEXT PRIMARY KEY," + "COURSENAME TEXT NOT NULL,"
               + "TEACHER TEXT NOT NULL," + "ROOM TEXT NOT NULL," + "STARTDAY TEXT NOT NULL," + "ENDDAY TEXT NOT NULL)";
       sqLiteDatabase.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql1 = "DROP TABLE USER";
        sqLiteDatabase.execSQL(sql1);
        String sql2  = "DROP TABLE COURSE";
        sqLiteDatabase.execSQL(sql2);
        String sql3 = "DROP TABLE SCHEDULE";
        sqLiteDatabase.execSQL(sql3);
        onCreate(sqLiteDatabase);
    }
}
