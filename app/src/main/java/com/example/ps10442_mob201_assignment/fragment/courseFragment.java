package com.example.ps10442_mob201_assignment.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10442_mob201_assignment.Adapter.CourseAdapter;
import com.example.ps10442_mob201_assignment.DAO.CourseDAO;
import com.example.ps10442_mob201_assignment.R;
import com.example.ps10442_mob201_assignment.model.Course;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class courseFragment extends Fragment {
    RecyclerView recyclerView;
    CourseDAO courseDAO;
    CourseAdapter adapter;
    ArrayList<Course> list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.coursefragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Course");
        recyclerView = view.findViewById(R.id.courseCyclerview);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        courseDAO = new CourseDAO(getContext());
        list = new ArrayList<Course>();
        list = courseDAO.takeAll();
        adapter = new CourseAdapter(getContext(),list);
        recyclerView.setAdapter(adapter);
    }
}
