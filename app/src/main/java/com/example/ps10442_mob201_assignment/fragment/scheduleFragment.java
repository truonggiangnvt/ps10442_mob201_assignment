package com.example.ps10442_mob201_assignment.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ps10442_mob201_assignment.Adapter.ScheduleAdapter;
import com.example.ps10442_mob201_assignment.DAO.ScheduleDAO;
import com.example.ps10442_mob201_assignment.R;
import com.example.ps10442_mob201_assignment.model.Schedule;

import java.util.ArrayList;

public class scheduleFragment extends Fragment {
    RecyclerView recyclerView;
    ScheduleDAO scheduleDAO;
    ArrayList<Schedule> list;
    ScheduleAdapter scheduleAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rcSchedule);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        scheduleDAO = new ScheduleDAO(getContext());
        list = new ArrayList<Schedule>();
        list = scheduleDAO.getAll();
        scheduleAdapter = new ScheduleAdapter(getContext(),list);
        recyclerView.setAdapter(scheduleAdapter);
    }
}
