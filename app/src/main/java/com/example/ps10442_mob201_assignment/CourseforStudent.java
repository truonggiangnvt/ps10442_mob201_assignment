package com.example.ps10442_mob201_assignment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.ps10442_mob201_assignment.fragment.courseFragment;
import com.example.ps10442_mob201_assignment.fragment.examFragment;
import com.example.ps10442_mob201_assignment.fragment.scheduleFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CourseforStudent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coursefor_student);
        BottomNavigationView bottomna = findViewById(R.id.bottom_navigation);
        bottomna.setOnNavigationItemSelectedListener(navListener);
        loadFragment(new courseFragment());
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFrament = null;

                    switch (menuItem.getItemId()){
                        case R.id.bottomCourse:
                            selectedFrament = new courseFragment();
                            loadFragment(selectedFrament);
                            return true;
                        case R.id.bottomSchedule:
                            selectedFrament = new scheduleFragment();
                            loadFragment(selectedFrament);
                            return true;
                        case R.id.bottomExam:
                            selectedFrament = new examFragment();
                            loadFragment(selectedFrament);
                            return true;
                    }

                    return false;
                }
            };
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.framelayout_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
