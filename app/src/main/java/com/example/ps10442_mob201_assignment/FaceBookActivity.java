package com.example.ps10442_mob201_assignment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ps10442_mob201_assignment.DAO.UserDAO;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class FaceBookActivity extends AppCompatActivity {
    private LoginButton loginButton;
    private CircleImageView imageView;
    private TextView txtname;
    private TextView txtemail;
    private CallbackManager callbackManager;
    String image_url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_book);
        map();
        callbackManager = CallbackManager.Factory.create();
        setLoginButton();
        checkLoginStatus();
    }
    private void map(){
        loginButton = findViewById(R.id.loginfacebook);
        imageView = findViewById(R.id.profile_pic);
        txtname = findViewById(R.id.profile_name);
        txtemail = findViewById(R.id.profile_email);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken == null)
            {
                txtname.setText("");
                txtemail.setText("");
                imageView.setImageResource(0);
                Toast.makeText(FaceBookActivity.this,"User Logged out",
                        Toast.LENGTH_SHORT).show();
            } else {
                loadUserProfile(currentAccessToken);
            }
        }
    };
    private void loadUserProfile(AccessToken accessToken)
    {

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (object == null){
                        object = new JSONObject();
                    }
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String id = object.getString("id");

                    image_url = "https://graph.facebook.com/"+id+"/picture?type=normal";

                    txtemail.setText(email);
                    txtname.setText(first_name + " " + last_name);
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();

                    Glide.with(FaceBookActivity.this).load(image_url).into(imageView);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }
    private void checkLoginStatus(){
        if (AccessToken.getCurrentAccessToken() != null){
            loadUserProfile(AccessToken.getCurrentAccessToken());
            Thread thread = new Thread(){
                public void run(){
                    try {
                        super.run();
                        sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        String role = null;
                        UserDAO userDAO = new UserDAO(FaceBookActivity.this);
                        role = userDAO.takeRole(txtemail.getText().toString());

                        if (role.equals("Admin")){
                            startActivity(new Intent(FaceBookActivity.this,MainScreenForAdmin.class));
                        } else {
                            startActivity(new Intent(FaceBookActivity.this,MainScreen.class));
                        }
                    }
                }
            };
            thread.start();
        }

        }
    private void setLoginButton(){
        loginButton.setReadPermissions(Arrays.asList("user_status"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Thread thread = new Thread(){
                    public void run(){
                        try {
                            super.run();
                            sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            Intent intent = new Intent(FaceBookActivity.this,UserActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("emailUser",txtemail.getText().toString());
                            bundle.putString("nameUser",txtname.getText().toString());
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    }
                };
                thread.start();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }
    }

