package com.example.ps10442_mob201_assignment.DAO;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10442_mob201_assignment.database.DataBaseHelper;
import com.example.ps10442_mob201_assignment.model.User;

import java.util.ArrayList;

public class UserDAO {
    SQLiteDatabase database;
    DataBaseHelper dataBaseHelper;

    public UserDAO(Context context){
        dataBaseHelper = new DataBaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
    }
    public ArrayList<User> takeAll(){
        ArrayList<User> list = new ArrayList<User>();
        Cursor cursor = database.query("USER",null,null,null,null,null,null);
        while (cursor.moveToNext()){
            User user = new User();
            user.setEmailUser(cursor.getString(0));
            user.setNameUser(cursor.getString(1));
            user.setDateUser(cursor.getString(2));
            user.setPhoneNumber(Integer.parseInt(cursor.getString(3)));
            user.setRole(cursor.getString(4));
            list.add(user);
        }
        return list;
    }
    public String takeRole(String email){
        String role = null;
        Cursor cursor = database.rawQuery("SELECT ROLEUSER FROM USER WHERE EMAILUSER = '"+email+"'",null);
        while (cursor.moveToNext()){
            role = cursor.getString(0);
        }
        return role;

    }
    public long addUser(User user){
        ContentValues contentValues = new ContentValues();
        contentValues.put("EMAILUSER",user.getEmailUser());
        contentValues.put("NAMEUSER",user.getNameUser());
        contentValues.put("DATEUSER",user.getDateUser());
        contentValues.put("PHONEUSER",user.getPhoneNumber());
        contentValues.put("ROLEUSER",user.getRole());
        return database.insert("USER",null,contentValues);
    }
    public int editUser(User user){
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAMEUSER",user.getNameUser());
        contentValues.put("DATEUSER",user.getDateUser());
        contentValues.put("PHONEUSER",user.getPhoneNumber());
        contentValues.put("ROLEUSER",user.getRole());
        return database.update("USER",contentValues,"EMAILUSER=?",new String[]{user.getEmailUser()});
    }
    public int deleteUser(User user){
        return database.delete("USER","EMAILUSER=?",new String[]{user.getEmailUser()});
    }
}
