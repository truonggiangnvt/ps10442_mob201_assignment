package com.example.ps10442_mob201_assignment.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10442_mob201_assignment.database.DataBaseHelper;
import com.example.ps10442_mob201_assignment.model.Course;
import com.example.ps10442_mob201_assignment.model.User;

import java.util.ArrayList;

public class CourseDAO {
    SQLiteDatabase database;
    DataBaseHelper dataBaseHelper;

    public CourseDAO(Context context){
        dataBaseHelper = new DataBaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
    }
    public ArrayList<Course> takeAll(){
        ArrayList<Course> list = new ArrayList<Course>();
        Cursor cursor = database.query("COURSE",null,null,null,null,null,null);
        while (cursor.moveToNext()){
            Course course = new Course();
            course.setCourseId(cursor.getString(0));
            course.setCourseName(cursor.getString(1));
            course.setCourseTeacher(cursor.getString(2));
            course.setCourseRoom(cursor.getString(3));
            course.setCourseAmount(Integer.parseInt(cursor.getString(4)));
            course.setStartDay(cursor.getString(5));
            course.setEndDay(cursor.getString(6));
            list.add(course);
        }
        return list;
    }
    public String  takeIdCourse(String nameCourse, String teacher){
        String idCourse = null;
        Cursor cursor = database.rawQuery("SELECT COURSEID FROM COURSE WHERE COURSENAME = '"+ nameCourse +"' AND TEACHER = '"+teacher+"'",null);
        while (cursor.moveToNext()){
            idCourse = cursor.getString(0);
        }
        return idCourse;
    }
    public long addCourse(Course course){
        ContentValues contentValues = new ContentValues();
        contentValues.put("COURSEID",course.getCourseId());
        contentValues.put("COURSENAME",course.getCourseName());
        contentValues.put("TEACHER",course.getCourseTeacher());
        contentValues.put("ROOM",course.getCourseRoom());
        contentValues.put("AMOUNT",course.getCourseAmount());
        contentValues.put("STARTDAY",course.getStartDay());
        contentValues.put("ENDDAY",course.getEndDay());
        return database.insert("COURSE",null,contentValues);
    }
    public int updateCourse(Course course){
        ContentValues contentValues = new ContentValues();
        contentValues.put("AMOUNT",course.getCourseAmount());
        return database.update("COURSE",contentValues,"COURSEID=?",new String[]{course.getCourseId()});
    }
}
