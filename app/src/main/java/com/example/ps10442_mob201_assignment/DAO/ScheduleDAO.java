package com.example.ps10442_mob201_assignment.DAO;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10442_mob201_assignment.database.DataBaseHelper;
import com.example.ps10442_mob201_assignment.model.Course;
import com.example.ps10442_mob201_assignment.model.Schedule;

import java.util.ArrayList;

public class ScheduleDAO {
    SQLiteDatabase database;
    DataBaseHelper dataBaseHelper;
    public  ScheduleDAO(Context context){
        dataBaseHelper = new DataBaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
    }
    public ArrayList<Schedule> getAll(){
        ArrayList<Schedule> list = new ArrayList<Schedule>();
        Cursor cursor = database.query("SCHEDULE",null,null,null,null,null,null);
        while (cursor.moveToNext()){
            Schedule schedule = new Schedule();
            schedule.setCourseId(cursor.getString(0));
            schedule.setCourseName(cursor.getString(1));
            schedule.setCourseTeacher(cursor.getString(2));
            schedule.setCourseRoom(cursor.getString(3));
            schedule.setStartDay(cursor.getString(4));
            schedule.setEndDay(cursor.getString(5));
            list.add(schedule);
        }
        return list;
    }
    public long addSchedule(Schedule schedule){
        ContentValues contentValues = new ContentValues();
        contentValues.put("COURSEID",schedule.getCourseId());
        contentValues.put("COURSENAME",schedule.getCourseName());
        contentValues.put("TEACHER",schedule.getCourseTeacher());
        contentValues.put("ROOM",schedule.getCourseRoom());
        contentValues.put("STARTDAY",schedule.getStartDay());
        contentValues.put("ENDDAY",schedule.getEndDay());
        return database.insert("SCHEDULE",null,contentValues);
    }
}
