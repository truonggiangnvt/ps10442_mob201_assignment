package com.example.ps10442_mob201_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainScreen extends AppCompatActivity {
    ImageView imgNews, imgCourse, imgMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        map();
        setImgNews();
        setImgMaps();
        setImgCourse();
    }
    private void map(){
        imgNews = findViewById(R.id.imgNews);
        imgCourse = findViewById(R.id.imgCourse);
        imgMaps = findViewById(R.id.imgMaps);

    }
    private void setImgNews(){
        imgNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainScreen.this,NewsActivity.class));
            }
        });
    }
    private void setImgMaps(){
        imgMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainScreen.this,MapsActivity.class));
            }
        });
    }
    private void setImgCourse(){
        imgCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainScreen.this,CourseforStudent.class));
            }
        });
    }


}
