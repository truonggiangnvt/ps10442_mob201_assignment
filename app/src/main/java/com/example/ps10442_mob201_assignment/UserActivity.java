package com.example.ps10442_mob201_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ps10442_mob201_assignment.DAO.UserDAO;
import com.example.ps10442_mob201_assignment.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserActivity extends AppCompatActivity {
    EditText edtEmailUser, edtNameUser, edtPhoneUser;
    TextView txtChooseDate;
    Button btnAccept;
    CircleImageView circleImageView;
    Calendar calendar = Calendar.getInstance();
    Spinner spinner;
    ArrayList<String> list = new ArrayList<String>();
    String character;
    UserDAO userDAO;
    ArrayList<User> arrayList;
    String url_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        map();
        Bundle bundle = getIntent().getExtras();
        String emailUser = bundle.getString("emailUser");
        String nameUser = bundle.getString("nameUser");
        url_image = bundle.getString("image");
        edtEmailUser.setText(emailUser);
        edtNameUser.setText(nameUser);
        Glide.with(UserActivity.this).load(url_image).into(circleImageView);
        setSpinner();
        setTxtChooseDate();
        userDAO = new UserDAO(this);
        arrayList = new ArrayList<>();
        setBtnAccept();
    }
    private void map(){
        edtEmailUser = findViewById(R.id.edtEmailUser);
        edtNameUser = findViewById(R.id.edtNameUser);
        edtPhoneUser = findViewById(R.id.edtPhoneUser);
        txtChooseDate = findViewById(R.id.tvDateUser);
        btnAccept = findViewById(R.id.btnAccept);
        circleImageView = findViewById(R.id.circleImageUser);
        spinner = findViewById(R.id.spinnerRole);
    }
    private void setSpinner(){
        list.add("Admin");
        list.add("User");
        ArrayAdapter adapter = new ArrayAdapter(UserActivity.this,android.R
                .layout.simple_list_item_1,list);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                character = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void setBtnAccept(){
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                if (!edtEmailUser.getText().toString().equals(edtEmailUser.getText().toString())){
                    edtEmailUser.setText(edtEmailUser.getText().toString());
                    edtEmailUser.setError("Email is not update");
                    return;
                }
                if (!validate()){
                    return;
                }
                user.setEmailUser(edtEmailUser.getText().toString());
                user.setNameUser(edtNameUser.getText().toString());
                user.setPhoneNumber(Integer.parseInt(edtPhoneUser.getText().toString()));
                user.setRole(character);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                user.setDateUser(simpleDateFormat.format(calendar.getTime()));
                if (userDAO.addUser(user)==-1){
                    Toast.makeText(UserActivity.this,"Email is duplicate",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(UserActivity.this,
                        "Insert Succsess",Toast.LENGTH_SHORT).show();
                if (character.equals("Admin")){
                    startActivity(new Intent(UserActivity.this,MainScreenForAdmin.class));
                } else {
                    startActivity(new Intent(UserActivity.this,MainScreen.class));
                }

            }
        });
    }
    private void setTxtChooseDate(){
        txtChooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseDate();
            }
        });
    }
    private void chooseDate(){
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datepicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                txtChooseDate.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, nam, thang, ngay
        );
        datepicker.show();
    }
    private boolean validate(){
        String name = edtNameUser.getText().toString().trim();
        String isnumber = "^[0-9]*$";
        String phone = edtPhoneUser.getText().toString().trim();
        if (name.isEmpty()){
            edtNameUser.setError("Name is not empty");
            return false;
        }
        if (name.matches(isnumber)){
            edtNameUser.setError("Name is not number");
            return false;
        }
        if (phone.isEmpty()){
            edtPhoneUser.setError("Phone is not empty");
            return false;
        }
        if (!phone.matches(isnumber)){
            edtPhoneUser.setError("Phone must a number");
            return false;
        }
        return true;
    }
}
