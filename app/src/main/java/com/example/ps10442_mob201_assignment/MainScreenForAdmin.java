package com.example.ps10442_mob201_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ps10442_mob201_assignment.DAO.CourseDAO;
import com.example.ps10442_mob201_assignment.model.Course;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainScreenForAdmin extends AppCompatActivity {
    ImageView imgWatch, imgAddCourse;
    Dialog myDiaLog;
    EditText edtCourseId, edtCourseName, edtCourseTeacher, edtSourseRoom, edtSourseAmount;
    TextView txtStartDay, txtEndDay;
    Button btnAddCourse;
    CourseDAO courseDAO;
    Calendar calendar = Calendar.getInstance();
    Calendar calendar1 = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen_for_admin);
        map();
        setImgWatch();
        myDiaLog = new Dialog(this);
        myDiaLog.setContentView(R.layout.dialog_course);
        courseDAO = new CourseDAO(this);
        setImgAddCourse();
    }
    private void map(){
        imgWatch = findViewById(R.id.imageWatch);
        imgAddCourse = findViewById(R.id.imageAddCourse);
    }
    private void setImgWatch(){
        imgWatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainScreenForAdmin.this,WatchListUser.class));
            }
        });
    }
    private  void setImgAddCourse(){
        imgAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtCourseId = myDiaLog.findViewById(R.id.edtCourseID);
                edtCourseName = myDiaLog.findViewById(R.id.edtCourseName);
                edtCourseTeacher = myDiaLog.findViewById(R.id.edtTeacher);
                edtSourseRoom = myDiaLog.findViewById(R.id.edtRoom);
                edtSourseAmount = myDiaLog.findViewById(R.id.edtAmount);
                txtStartDay = myDiaLog.findViewById(R.id.tvStartDay);
                txtEndDay = myDiaLog.findViewById(R.id.txtEndDay);
                btnAddCourse = myDiaLog.findViewById(R.id.btnAddCourse);
                myDiaLog.show();
                btnAddCourse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Course course = new Course();
                        if (!validate()){
                            return;
                        }
                        course.setCourseId(edtCourseId.getText().toString());
                        course.setCourseName(edtCourseName.getText().toString());
                        course.setCourseTeacher(edtCourseTeacher.getText().toString());
                        course.setCourseRoom(edtSourseRoom.getText().toString());
                        course.setCourseAmount(Integer.parseInt(edtSourseAmount.getText().toString()));
                        course.setStartDay(txtStartDay.getText().toString());
                        course.setEndDay(txtEndDay.getText().toString());
                        if (courseDAO.addCourse(course)==-1){
                            Toast.makeText(MainScreenForAdmin.this,"CourseID is duplicate",
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(MainScreenForAdmin.this,"Insert Success",
                                Toast.LENGTH_SHORT).show();
                        edtCourseId.setText("");
                        edtCourseName.setText("");
                        edtCourseTeacher.setText("");
                        edtSourseRoom.setText("");
                        edtSourseAmount.setText("");
                        txtStartDay.setText("ChooseDate");
                        txtEndDay.setText("ChooseDate");
                    myDiaLog.hide();


                    }
                });
                txtStartDay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseDay();
                    }
                });
                txtEndDay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseDay1();
                    }
                });
            }
        });
    }


    private void chooseDay() {
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datepicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                txtStartDay.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, nam, thang, ngay
        );
        datepicker.show();
    }
    private void chooseDay1() {
        int ngay = calendar1.get(Calendar.DATE);
        int thang = calendar1.get(Calendar.MONTH);
        int nam = calendar1.get(Calendar.YEAR);
        DatePickerDialog datepicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar1.set(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                txtEndDay.setText(simpleDateFormat.format(calendar1.getTime()));
            }
        }, nam, thang, ngay
        );
        datepicker.show();
    }
    private boolean validate(){
        String id = edtCourseId.getText().toString().trim();
        String name = edtCourseName.getText().toString().trim();
        String teacher = edtCourseTeacher.getText().toString().trim();
        String room = edtSourseRoom.getText().toString().trim();
        String amount = edtSourseAmount.getText().toString().trim();
        String isnumber = "^[0-9]*$";
        if (id.isEmpty()){
            edtCourseId.setError("Id is not empty");
            return false;
        }
        if (name.isEmpty()){
            edtCourseName.setError("Name is not empty");
            return false;
        }
        if (name.matches(isnumber)){
            edtCourseName.setError("Name is not number");
            return false;
        }
        if (room.isEmpty()){
            edtSourseRoom.setError("Room is not empty");
            return false;
        }
        if (amount.isEmpty()){
            edtSourseAmount.setError("Amount is not empty");
            return false;
        }
        if (!amount.matches(isnumber)){
            edtSourseAmount.setError("Amount must be a number");
        }
        if (teacher.isEmpty()){
            edtCourseTeacher.setError("Teacher is not empty");
            return false;
        }
        if (teacher.matches(isnumber)){
            edtCourseTeacher.setError("Teacher must not be number");
            return false;
        }
        return true;
    }

}
