package com.example.ps10442_mob201_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ps10442_mob201_assignment.Adapter.UserAdapter;
import com.example.ps10442_mob201_assignment.DAO.UserDAO;
import com.example.ps10442_mob201_assignment.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class WatchListUser extends AppCompatActivity {
    ListView listView;
    UserAdapter userAdapter;
    UserDAO userDAO;
    ArrayList<User> list;
    Dialog myDialog;
    TextView txtEditEmailUser, txtEditDateUser;
    EditText edtEditNameUser, edtEditPhoneUser;
    Spinner spinnerSetRole;
    Button btnEdit, btnDel;
    Calendar calendar = Calendar.getInstance();
    String character = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_list_user);
        listView = findViewById(R.id.lvWatch);
        userDAO = new UserDAO(this);
        list = new ArrayList<>();
        list = userDAO.takeAll();
        userAdapter = new UserAdapter(this,list);
        listView.setAdapter(userAdapter);
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.my_dialog);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                txtEditEmailUser = myDialog.findViewById(R.id.txtEditEmailUser);
                edtEditNameUser = myDialog.findViewById(R.id.txtEditNameUser);
                edtEditPhoneUser = myDialog.findViewById(R.id.txtEditPhoneUser);
                txtEditDateUser = myDialog.findViewById(R.id.tvEditDate);
                spinnerSetRole = myDialog.findViewById(R.id.spinnerEditUser);
                btnEdit = myDialog.findViewById(R.id.btnEditUser);
                btnDel = myDialog.findViewById(R.id.btnDelUser);
                final User user = list.get(i);
                edtEditNameUser.setText(user.getNameUser());
                edtEditPhoneUser.setText(String.valueOf(user.getPhoneNumber()));
                txtEditDateUser.setText(user.getDateUser());
                final ArrayList<String> list = new ArrayList<String>();
                list.add(user.getRole());
                list.add("User");
                final ArrayAdapter adapter = new ArrayAdapter(WatchListUser.this,android.R.layout.simple_list_item_1,list);
                spinnerSetRole.setAdapter(adapter);
                spinnerSetRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        character = spinnerSetRole.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                txtEditDateUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseDate();
                    }
                });
                myDialog.show();
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!validate()){
                            return;
                        }
                        user.setNameUser(edtEditNameUser.getText().toString());
                        user.setPhoneNumber(Integer.parseInt(edtEditPhoneUser.getText().toString()));
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        user.setDateUser(simpleDateFormat.format(calendar.getTime()));
                        user.setRole(character);
                        UserDAO userDAO = new UserDAO(WatchListUser.this);
                        if (userDAO.editUser(user)==-1){
                            Toast.makeText(WatchListUser.this,
                                    "Fail Update",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(WatchListUser.this,
                                "Success Update",Toast.LENGTH_SHORT).show();
                        myDialog.hide();
                        userAdapter.notifyDataSetChanged();
                    }
                });
                btnDel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (userDAO.deleteUser(user)==-1){
                            Toast.makeText(WatchListUser.this,
                                    "Fail Delete",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(WatchListUser.this,
                                "Success Delete",Toast.LENGTH_SHORT).show();
                        list.remove(i);
                        myDialog.hide();
                    }
                });
            }
        });

    }
    private void chooseDate(){
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datepicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                txtEditDateUser.setText(simpleDateFormat.format(calendar.getTime()));
            }
        }, nam, thang, ngay
        );
        datepicker.show();
    }
    private boolean validate(){
        String name = edtEditNameUser.getText().toString().trim();
        String phone = edtEditPhoneUser.getText().toString().trim();
        String isnumber = "^[0-9]*$";

        if (name.isEmpty()){
            edtEditNameUser.setError("Name is not empty");
            return false;
        }
        if (name.matches(isnumber)){
            edtEditNameUser.setError("Name is not number");
            return false;
        }
        if (phone.isEmpty()){
            edtEditPhoneUser.setError("Phone is not empty");
            return false;
        }
        if (!phone.matches(isnumber)){
            edtEditPhoneUser.setError("Phone must be a number");
            return false;
        }
        return true;
    }
}
